#include <iostream>
#include <string>
using namespace std;

int main()
{
	//here is the comment :)
	int hunger = 0;
	int health = 100;
	int happiness = 100;
	string petName = "";

	bool isDone = false;
	int menuChoice;
	int quitInput;
	int foodInput;
	int playInput;

	cout << "Please enter your pet's name: ";
	cin >> petName;

	while(!isDone)
	{
		cout << "--------------------------------------------------------------" << endl;
		cout << petName << endl;
		cout << "\t Hunger: " << hunger << "%";
		cout << "\t Health: " << health << "%";
		cout << "\t Happiness: " << happiness << "%" << endl;
		cout << "--------------------------------------------------------------" << endl;

		hunger += 5;
		if (hunger > 50)
		{
			happiness -= 10;
			health -= 10;
		}else {
			happiness -= 5;
		}
		cout << "Options: 1. Feed  2. Play  3. Vet  4. Quit" << endl;
		cout << "> ";
		cin >> menuChoice;

		if (happiness < 0)
		{
			happiness = 0;
		} else if(happiness > 100) {
			happiness = 100;
		}

		if (hunger < 0)
		{
			hunger = 0;
		}
		else if (hunger > 100) {
			hunger = 100;
		}

		if (health < 0)
		{
			health = 0;
			cout << "You haven't taken care of " << petName << "!" << endl;
			cout << petName << " has been removed from your care." << endl;
			isDone = true;
		}
		else if (health > 100) {
			health = 100;
		}
		
		switch(menuChoice){

		case 1:
			cout << "FOODS: 1. Pizza  2. Broccoli  3. Tuna" << endl;
			cin >> foodInput;
			switch (foodInput) {
				case 1:
					cout << "You feed pizza to " << petName << endl;
					health -= 1;
					hunger -= 15;
					break;
				case 2:
					cout << "You feed broccoli to " << petName << endl;
					health += 1;
					hunger -= 10;
					break;
				case 3:
					cout << "You feed tuna to " << petName << endl;
					health += 1;
					hunger -= 10;
					break;
			}
			break;

		case 2:
			cout << "OPTIONS: 1. Fetch  2. Tug-of-War  3. Videogame" << endl;
			cin >> playInput;
			switch(playInput) {
				case 1:
					cout << "You play fetch with " << petName << endl;
					happiness += 8;
					health += 2;
				break;

				case 2:
					cout << "You play fetch tug-of-war with " << petName << endl;
					happiness += 9;
				break;

				case 3:
					cout << "You play videogames with " << petName << endl;
					health -= 1;
					happiness += 10;
				break;
			}
			break;

		case 3:
			if (happiness >= 50, hunger >= 50, health >= 50) {
				cout << petName << " is looking okay!" << endl;
			}
			else {
				if (happiness < 50) {
					cout << "Make sure to play with " << petName << " more." << endl;
				}
				if (hunger < 50) {
					cout << "Make sure to feed " << petName << "." << endl;
				}
				if (health < 50) {
					cout << petName << " isn't looking very healthy. Take better care of it!" << endl;
				}
			}
			break;
		case 4:
			cout << "Are you sure you want to quit?" << endl;
			cout << "1. QUIT" << endl;
			cout << "2. Don't Quit" << endl;
			cout << "> ";
			cin >> quitInput;
			switch (quitInput) {
				case 1:
					isDone = true;
				break;
				
				case 2:
					isDone = false;
				break;
			}
			break;
		}
	}

	return 0;
}
